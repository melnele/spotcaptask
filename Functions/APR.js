const MAX_ITERATION = 100;
const MIN_PRECISION = 1e-10; // .0000000001

// We assume that the first  repayment  is  made  one  month  after  the  start  of  the  agreement.
var NPV = (r, data) => {
    const arr = data.schedule;
    var d1 = (new Date(arr[0].date) / (1000 * 3600 * 24)) - (365.25 / 12);
    var sum = (-1 * data.principal) + data.upfrontFee.value;

    for (let i = 0; i < arr.length; i++) {
        const d2 = new Date(arr[i].date) / (1000 * 3600 * 24);
        const t = (d2 - d1) / 365.25;

        sum += ((arr[i].principal + arr[i].interestFee) / Math.pow((1 + r), t));
    }
    return sum;
}

var NPVd = (r, data) => {
    const arr = data.schedule;
    var d1 = (new Date(arr[0].date) / (1000 * 3600 * 24)) - (365.25 / 12);
    var sum = 0;

    for (let i = 0; i < arr.length; i++) {
        const d2 = new Date(arr[i].date) / (1000 * 3600 * 24);
        const t = (d2 - d1) / 365.25;

        sum -= (((arr[i].principal + arr[i].interestFee) * t) / Math.pow((1 + r), t + 1));
    }
    return sum;
}

var R1 = (data) => {
    var x = -1 * ((-1 * data.principal) + data.upfrontFee.value);
    var sum = data.schedule.reduce((y, z) => (y + z.principal + z.interestFee - 1), 0)
    return sum / x;
}

function APR(data) {
    var r1 = R1(data);

    for (let i = 0; i < MAX_ITERATION; i++) {
        var npv = NPV(r1, data);
        var npvd = NPVd(r1, data);
        var r2 = r1 - (npv / npvd);

        if (Math.abs(r2 - r1) < MIN_PRECISION)
            return Math.round(r2 * 1000) / 10;
        r1 = r2;
    }
    return Math.round(r2 * 1000) / 10;
}

module.exports = APR