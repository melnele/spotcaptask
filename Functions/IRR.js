const MAX_ITERATION = 100;
const MIN_PRECISION = 1e-10; // .0000000001

var NPV = (r, data) => ((-1 * data.principal) + data.upfrontFee.value + data.schedule.reduce((y, z, i) => {
    return y + ((z.principal + z.interestFee) / Math.pow((1 + r), i + 1))
}, 0));

function IRR(data) {
    const A = data.schedule.reduce((y, z) => (y + z.principal + z.interestFee), 0);
    const C = Math.abs((-1 * data.principal) + data.upfrontFee.value);
    const AC = A / C;
    const npmi = data.schedule.reduce((y, z, i) => (y + ((z.principal + z.interestFee) / Math.pow(2, i + 1))), 0);
    const p1 = (2 / (data.schedule.length + 1));
    const p2 = Math.log(AC) / Math.log(A / npmi);
    var r1 = Math.pow((AC), p1) - 1;
    var r2 = Math.pow((AC), p2) - 1;

    for (let i = 0; i < MAX_ITERATION; i++) {
        var npv1 = NPV(r1, data);
        var npv2 = NPV(r2, data);
        var rt = r2 - (npv2 * ((r2 - r1) / (npv2 - npv1)));
        r1 = r2;
        r2 = rt;
        if (Math.abs(r2 - r1) < MIN_PRECISION)
            return Number(r2.toFixed(10));
    }
    return Number(r2.toFixed(10));
}

module.exports = IRR