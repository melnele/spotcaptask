const express = require('express');
const createError = require('http-errors');
const Joi = require('@hapi/joi');
const APR = require('../Functions/APR');
const IRR = require('../Functions/IRR');

var router = express.Router();

const schedule = Joi.array().items(Joi.object({
  id: Joi.number().integer().min(1).required(),
  date: Joi.date().required(),
  principal: Joi.number().integer().min(0).required(),
  interestFee: Joi.number().integer().min(0).required()
}));

const schema = Joi.object({
  principal: Joi.number().integer().min(0).required(),
  upfrontFee: Joi.object({ value: Joi.number().integer().min(0).required() }),
  upfrontCreditlineFee: Joi.object({ value: Joi.number().integer().min(0) }),
  schedule
})

router.post('/rates', async function (req, res, next) {
  try {
    const value = await schema.validateAsync(req.body);
    console.log(value);

    var apr = APR(req.body);
    var irr = IRR(req.body);
    res.send({ apr, irr });
  }
  catch (err) {
    console.log(err);
    res.send(createError(400));
  }

});

module.exports = router;