const expect = require('chai').expect;
const data = require('./example.json');
const APR = require('../Functions/APR');
const IRR = require('../Functions/IRR');

describe('Checks if Rates functions works', function () {
    it('Checks if APR is correct', () => {
        const apr = APR(data.input);
        expect(apr).to.equal(data.output.apr);
    });
    it('Checks if IRR is correct', () => {
        const irr = IRR(data.input);
        expect(irr).to.equal(data.output.irr);
    });
});